package juliano.com.tddjava;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.*;

class CompraTest {
	
	@BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }
	
	@Test
	void valida_CompraSaldoOk_XPOk() {
		
		Compra compra = new Compra();
		Cliente cliente = new Cliente (45, "Joao Silva");
		Produto produto = new Produto(123, 100.00 , "frutas");
		
		// XPconsult xpconsult = new XPconsult();		
		XPconsult mockedXPconsult = mock(XPconsult.class);	    
	    when(mockedXPconsult.consultaScore(cliente)).thenReturn(true);
	    
		compra.addItemCarrinho(produto);
		cliente.setSaldo(200);
		
		System.out.println( "\n -- TESTE -- Compra OK : true " );		
		assertTrue ( compra.aprovarCompra(cliente, mockedXPconsult) );
	
	}

	@Test
	void valida_CompraSemSaldo_XPOk() {
		
		Compra compra = new Compra();
		Cliente cliente = new Cliente (26, "Maria Cardoso");
		Produto produto = new Produto(123, 100.00 , "frutas");
		
		// XPconsult xpconsult = new XPconsult();		
		XPconsult mockedXPconsult = mock(XPconsult.class);	    
	    when(mockedXPconsult.consultaScore(cliente)).thenReturn(true);
		
		compra.addItemCarrinho(produto);
		cliente.setSaldo(50);
		
		System.out.println( "\n -- TESTE -- Compra Erro (sem saldo) : false " );
		assertFalse ( compra.aprovarCompra(cliente, mockedXPconsult) );
	
	}

	
}
