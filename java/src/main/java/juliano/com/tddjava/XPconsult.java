package juliano.com.tddjava;

import java.util.Random;

public class XPconsult {

	// private String url = "www.xp.com.br"; 
	
	public boolean consultaScore ( Cliente cliente  ) {
		
		double id = cliente.getId();
		double score = 0; 
		
		Random gerador = new Random();
		score = gerador.nextDouble();
        
		System.out.println( " -- DEBUG -- Cliente: " + id + " Score: " + score + "(0.75)"  );
		
		return (score > 0.75 );
		
	}
	
}
