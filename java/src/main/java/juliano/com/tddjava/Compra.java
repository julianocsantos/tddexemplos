package juliano.com.tddjava;

import java.util.ArrayList;
import java.util.List;

public class Compra {

	private int id; 
	private double total; 
	
	List<Produto> carrinho = new ArrayList<Produto>();
	
	public int getId() {
		return id;
	}
	
	public double getTotal() {
		return total;
	}
	
	public void addItemCarrinho(Produto produto) {
		total += produto.getValor();
		this.carrinho.add(produto);
	}
	
	public boolean aprovarCompra ( Cliente cliente, XPconsult xpconsult  ) {
		
		double saldo = cliente.getSaldo();
		double compra = this.getTotal();
		
		System.out.println( " -- DEBUG -- Total da Compra: " + compra + " - Saldo Cliente: " + saldo );
				
		return ( (cliente.getSaldo() >= this.getTotal() ) & xpconsult.consultaScore(cliente) );
		
	}
	    
}
