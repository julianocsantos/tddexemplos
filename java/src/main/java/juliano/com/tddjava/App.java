package juliano.com.tddjava;

/**
 * App de Teste!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	
        System.out.println( " ======= Sistema de Vendas ===== " );
        
        Compra compra = new Compra();
        
		Cliente cliente = new Cliente (45, "Joao Silva");
		cliente.setSaldo(200);
				
		Produto produto = new Produto(239, 47.00 , "frutas");
		Produto produto2 = new Produto(764, 78.00 , "carne");
		compra.addItemCarrinho(produto);
		compra.addItemCarrinho(produto2);
		
		XPconsult xpconsult = new XPconsult();
		
		if ( compra.aprovarCompra(cliente, xpconsult) ) {
			System.out.println( " -->> Venda Aprovada !" );
		} else {
			System.out.println( " -->> Venda Cancelada !" );
		}	
        
    }
}

