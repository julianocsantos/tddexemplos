package juliano.com.tddjava;

public class Cliente {
	
	// Informações dos clientes
	private int id;
	private String nome;   
	private double saldo;
	
	public Cliente(int id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
		this.saldo = 0.0;
	}

	public int getId() {
		return id;
	}
		
	public String getNome() {
		return nome;
	}
			
	public double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	} 
	
}
